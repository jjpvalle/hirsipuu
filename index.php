<?php
session_start();
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        //$sanat = array('kissa','koira','muurahaiskarhu');
        $file = fopen("sanat.csv","r");
        $sanat = fgetcsv($file,0,';');
        fclose($file);
        
        
        $arvatut='';
        $arvattava='';
        $piilotettu='';
        
        if ($_SERVER['REQUEST_METHOD']==='GET') {
            $arvattava = $sanat[rand(0,count($sanat)-1)];
            $_SESSION['arvattava'] = $arvattava;
            $piilotettu = str_repeat('*', strlen($arvattava));
            $_SESSION['arvatut']='';
        }
        else if ($_SERVER['REQUEST_METHOD']==='POST') {
            $arvaus = filter_input(INPUT_POST,'arvaus',FILTER_SANITIZE_STRING);
            //$arvatut = filter_input(INPUT_POST,'arvatut',FILTER_SANITIZE_STRING);
            $arvatut = $_SESSION['arvatut'];
            //$arvattava = filter_input(INPUT_POST,'arvattavasana',FILTER_SANITIZE_STRING);
            $arvattava = $_SESSION['arvattava'];
            $piilotettu = filter_input(INPUT_POST,'arvattava',FILTER_SANITIZE_STRING);
            if (strlen($arvaus)===1) {
                //$arvatut.=$arvaus . "&nbsp;";
                
                $arvatut = $arvatut . $arvaus . "&nbsp;";
                $_SESSION['arvatut'] = $arvatut;
                for ($i=0;$i<strlen($piilotettu);$i++) {
                    $paikka = stripos($arvattava,$arvaus,$i);
                    if ($paikka!==false) {
                        $piilotettu = substr_replace($piilotettu, $arvaus,$paikka, 1);
                        $i = $paikka;
                    }
                }
                
            }
            else {
                //echo "Käyttäjä syötti koko sanan! arvaus oli $arvaus ja arvattava oli $arvattava";
                if ($arvaus===$arvattava) {
                    $piilotettu = $arvattava;
                    //echo "Sanat oli samat!";
                }
            }
            
            if ($arvattava===$piilotettu) {
                print "<p>Arvasit sanan oikein</p>";
                print "<a href='" . $_SERVER['PHP_SELF'] . "'>Uusi peli</a>";
            }
            
        }
        ?>
        <h3>Hirsipuu</h3>
        <form action="<?php print($_SERVER['PHP_SELF']);?>" method="post">
            <!--<input name="arvatut" value="<?php //print $arvatut;?>" type="hidden">-->
            <!--<input name="arvattavasana" value="<?php //print $arvattava; ?>" type="hidden">-->
            <div>
                <label>Arvattava sana</label>
                <input name="arvattava" value="<?php print $piilotettu; ?>" />
            </div>
            <div>
                <label>Kirjain tai sana</label>
                <input name="arvaus" />
            </div>
            <button>Arvaa</button>
        </form>
        <?php print "Arvatut kirjaimet $arvatut";?>
    </body>
</html>
